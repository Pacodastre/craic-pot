# Craic Pot Mobile

## Description

Craic Pot Mobile is the mobile application for the Craic Pot poker game. It is written in TypeScript with the react-native/expo framework.

## Setup

To run the mobile application on the desktop, you will need to install:

- You will need to install watchman.
  On archlinux, it can be installed with `yay -S watchman`.
- Install [Android Studio and follow the instructions](https://docs.expo.dev/workflow/android-studio-emulator/).
  On archlinux, it can be installed with `yay -S android-studio`.

* [Install asdf](https://asdf-vm.com/guide/getting-started.html), and then run `asdf install` from this directory to install the node version specified in the `.tool-versions` file.
* Run `yarn` to install all the project dependencies.

## Usage

Start the development server with:

    yarn expo start

## License

Craic Pot is [AGPL licensed](../LICENSE.md).
