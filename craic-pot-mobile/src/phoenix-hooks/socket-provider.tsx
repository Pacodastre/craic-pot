import { Socket, Channel } from "phoenix";
import {
  useState,
  useEffect,
  useContext,
  createContext,
  PropsWithChildren,
} from "react";

const SocketContext = createContext<Socket | null>(null);

export const useSocket = () => useContext(SocketContext);

type SocketProviderProps = {
  socketURL: string;
} & PropsWithChildren;

export const SocketProvider = ({
  socketURL,
  children,
}: SocketProviderProps) => {
  const socket = useSetupSocket(socketURL);

  return (
    <SocketContext.Provider value={socket}>{children}</SocketContext.Provider>
  );
};

export const useSetupSocket = (socketURL: string) => {
  const [stateSocket, setStateSocket] = useState<Socket | null>(null);

  useEffect(() => {
    const socket = new Socket(socketURL);
    setStateSocket(socket);

    const connect = () => {
      console.debug("Connecting...");

      socket.onError((error) =>
        console.error("Error connecting to the server: ", error),
      );
      socket.onMessage((message: object) =>
        console.debug(`Message received: ${JSON.stringify(message)}`),
      );

      socket.connect();
    };
    connect();

    const disconnect = () => {
      console.debug("Disconnecting...");
      socket.disconnect();
    };

    return disconnect;
  }, [socketURL]);

  return stateSocket;
};

export const useChannel = (channelName: string) => {
  const socket = useSocket();
  const [stateChannel, setStateChannel] = useState<Channel | null>(null);

  useEffect(() => {
    if (!socket) {
      return;
    }
    const channel = socket.channel(channelName);
    setStateChannel(channel);

    const join = () => {
      console.debug(`Joining channel: ${channelName}`);

      channel.onError((reason?: any) =>
        console.error(`Channel error: ${reason}`),
      );

      channel.join().receive("ok", (param) => {
        console.debug(`Joined channel ${channelName}`, param);
      });
    };

    join();

    return () => {
      channel.leave();
    };
  }, []);
  return stateChannel;
};
