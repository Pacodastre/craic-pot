defmodule CraicPotBackend.Repo do
  use Ecto.Repo,
    otp_app: :craic_pot_backend,
    adapter: Ecto.Adapters.Postgres
end
