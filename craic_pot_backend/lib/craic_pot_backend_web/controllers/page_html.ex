defmodule CraicPotBackendWeb.PageHTML do
  use CraicPotBackendWeb, :html

  embed_templates "page_html/*"
end
