defmodule CraicPotBackendWeb.Layouts do
  use CraicPotBackendWeb, :html

  embed_templates "layouts/*"
end
