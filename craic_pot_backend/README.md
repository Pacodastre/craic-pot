# Craic Pot Backend

## Description

Craic Port Backend is the backend application for the Craic Pot poker app. It is written in Elixir with the Phoenix framework.

## Setup

* Install `postgresql`. On archlinux, it can be installed with `yay postgresql`.
* [Install asdf](https://asdf-vm.com/guide/getting-started.html), and then run `asdf install` from this directory to install the elixir and erlang versions specified in the `.tool-versions` file.
* Run `mix setup` to install and setup dependencies

## Usage

Start Phoenix endpoint with:

    mix phx.server

or inside IEx with:

    iex -S mix phx.server

Now you can visit [`localhost:4000`](http://localhost:4000) from your browser.

