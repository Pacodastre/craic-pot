# Craic Pot

## Description

Craic Pot is an open source poker game written in Elixir (Phoenix framework) and React Native (Expo).

## Documentation

* [Mobile Application README](./craic-pot-mobile/README.md)

## License

Craic Pot is [AGPL licensed](./LICENSE.md).
